import numpy as np
import cv2

def filter_segm(marked, cutoff_ratio, maxval):
    good_sl = np.zeros_like(marked)
    bad_sl = np.zeros_like(marked)
    for i in xrange(marked.shape[2]):
        if np.sum(marked[:,:,i]) > (cutoff_ratio * maxval * 255):
            bad_sl[:,:,i]=marked[:,:,i]
        else:
            good_sl[:,:,i]=marked[:,:,i]
    return good_sl, bad_sl

def clean_up_segm(segm):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
    clean = []
    for sl in segm:
        opened = cv2.morphologyEx(sl, cv2.MORPH_OPEN, kernel, iterations=2)
        clean.append(cv2.erode(opened, kernel))
    return np.array(clean)