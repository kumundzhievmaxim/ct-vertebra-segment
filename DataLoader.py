import pydicom as dicom
import os
import numpy as np


def read_centers(file_path):
    ''' Reads in the template matching results from file

        Params:
        ---------
        file_path: string
            full path to file

        Returns:
        ---------
        c_arr: ndarray
            center coordinates
    '''

    c_arr = []
    try:
        with open(file_path, "r") as c_file:
            lines = c_file.readlines()
            for line in lines:
                x, y = line.split(',')
                c_arr.append([int(x), int(y)])
    except IOError:
        print "No center file found"
    return np.array(c_arr)


def make_center_file(centers, file_path):
    ''' Creates a file containing the result of template matching

        Params:
        ---------
        centers: ndarray
            result of template matching
        file_path: string
            full path to file
    '''

    with open(file_path, "w") as c_file:
        for c in centers:
            c_file.write("{},{}\n".format(c[0], c[1]))


def convert_volume(volume, init_th=0):
    ''' Converts raw pixel data to uint8, with optional thresholding

        Params:
        ---------
        volume: ndarray
            raw pixel data (mutable)
        init_th: int
            initial threshold value (recommended: 10000)
    '''

    print("Converting Volume...")
    print("Initial Threshold: "+str(init_th))
    volume[volume < init_th] = 0
    max_val = np.max(volume).astype(np.float32)
    volume[:, :, :] = (volume[:, :, :]/max_val)*255
    print("Converting: Done")


def load_folder_raw(folder_path):
    ''' Reads in a folder containing dicom slice files

        Params:
        ---------
        folder_path: string
            full path to containing folder

        Returns:
        ---------
        slice_img: ndarray
            raw pixel data
    '''

    print("Reading folder (raw): "+folder_path)

    slices = {}
    for dirName, _, fileList in os.walk(folder_path):
        for filename in fileList:
            if(".center" in filename):
                continue
            file_path = os.path.join(dirName, filename)
            ct_slice = dicom.read_file(file_path)
            slices[ct_slice.InstanceNumber] = file_path

    ref = dicom.read_file(slices[1])
    const_pixel_dims = (ref.Rows, ref.Columns, len(slices))
    volume = np.zeros(const_pixel_dims, dtype=np.int16)

    print("Sorting slices...")

    for key in sorted(slices.keys()):
        file_path = slices[key]
        slice_array = dicom.read_file(file_path).pixel_array.astype(np.int16)
        volume[:, :, key-1] = slice_array

    print("Reading: Done")
    return volume[:, :, :]


def load_slice_raw(file_path):
    ''' Reads in a dicom slice file

        Params:
        ---------
        file_path: string
            full path to dicom slice

        Returns:
        ---------
        slice_img: ndarray
            raw pixel data
    '''

    print("Reading slice: "+file_path)
    print dicom.read_file(file_path)
    slice_img = dicom.read_file(file_path).pixel_array.astype(np.int16)
    return slice_img


def load_folder(folder_path):
    ''' !DEPRECATED!
        ---------
        Use load_folder_raw instead
    '''

    print("Reading folder: "+folder_path)

    slices = {}
    for dirName, _, fileList in os.walk(folder_path):
        for filename in fileList:
            file_path = os.path.join(dirName, filename)
            ct_slice = dicom.read_file(file_path)
            slices[ct_slice.AcquisitionNumber] = file_path

    ref = dicom.read_file(slices[1])
    const_pixel_dims = (ref.Rows, ref.Columns, len(slices))
    volume = np.zeros(const_pixel_dims, dtype=np.uint8)

    print("Converting slices...")

    for key in sorted(slices.keys()):
        file_path = slices[key]
        slice_array = dicom.read_file(file_path).pixel_array.astype(np.float32)
        norm_slice_array = np.zeros_like(slice_array)
        max_value = np.max(slice_array)
        norm_slice_array[:, :] = slice_array[:, :]/max_value
        volume[:, :, key-1] = norm_slice_array*255

    print("Done")
    return volume[:, :, :]


def load_slice(file_path):
    ''' !DEPRECATED!
        ---------
        Use load_slice_raw instead
    '''

    print("Reading slice: "+file_path)
    ct_slice = dicom.read_file(file_path).pixel_array.astype(np.float32)
    max_value = np.max(ct_slice)
    norm_slice_array = np.zeros_like(ct_slice)
    norm_slice_array[:, :] = ct_slice[:, :] / max_value
    slice_array = np.zeros_like(norm_slice_array, dtype=np.uint8)
    slice_array[:, :] = norm_slice_array * 255
    return slice_array
