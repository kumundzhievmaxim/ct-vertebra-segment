import numpy as np
import cv2
import matplotlib.pyplot as plt


def adjust_contrast(img):
    '''Contrast adjustment like in Photoshop:Curves'''

    # parameter optimization possible
    min_mean = img - np.mean(img)
    min_mean[min_mean < 0] = 0
    max_int = (np.max(img)-np.mean(img)).astype(np.float)+0.01
    scaled = (min_mean/max_int)*10-5
    e_scaled = np.maximum(scaled, 2)
    return (((np.tanh(scaled*e_scaled)+1)/2)*255).astype(np.uint8)


def match_vert_template(img, rad=70):
    '''Finds potential vertebral bodies with template matching'''

    # parameter optimization possible
    blur = cv2.GaussianBlur(img, (7, 7), 10)
    g_adj = adjust_contrast(blur)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    template = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (rad, rad))
    template = np.pad(template, (5, 5), 'constant', constant_values=0)
    erod = cv2.erode(template, kernel)
    template = template
    template = template-erod
    match = cv2.matchTemplate(g_adj, template, cv2.TM_CCOEFF)
    # plt.matshow(img, cmap=plt.cm.gray)
    # plt.show()
    # plt.matshow(g_adj, cmap=plt.cm.gray)
    # plt.show()
    # plt.matshow(match, cmap=plt.cm.gray)
    # plt.show()
    _, _, _, maxloc = cv2.minMaxLoc(match)
    center = (maxloc[0]+template.shape[0]/2, maxloc[1]+template.shape[1]/2)
    return center


def match_all_template(volume, rad):
    '''Batch run template matching'''

    centers = []
    for i in xrange(volume.shape[2]):
        C = match_vert_template(volume[:, :, i], rad)
        centers.append(C)
        print str(i)+"/"+str(volume.shape[2]-1)+":"+str(C)

    return np.array(centers)


def windowed_avg_diff(cents, win_size):
    '''Average difference with sliding windows'''

    win_avg_diff = []
    for c in xrange(len(cents)-win_size):
        window = cents[c:c+win_size]
        diff_sum = 0
        for i in range(len(window)):
            if(i < len(window)-1):
                diff_sum += cv2.norm(tuple(window[i]), tuple(window[i+1]), cv2.NORM_L2)
        win_avg_diff.append(diff_sum/win_size)
    return np.array(win_avg_diff)


def diff_from_centroid(points):
    '''Calculates centroid from points and their difference from it'''

    x = [p[0] for p in points]
    y = [p[1] for p in points]
    centroid = (sum(x) / len(points), sum(y) / len(points))
    diffs = [cv2.norm(tuple(p), centroid, cv2.NORM_L2) for p in points]
    return np.array(diffs)


def reshape_seeds(seed_ind):
    '''Reshape seed array to an array of lists for each vertebra'''

    resh = []
    row = []
    for i in xrange(len(seed_ind)-1):
        if seed_ind[i+1]-seed_ind[i] < 3:
            row.append(seed_ind[i])
        else:
            row.append(seed_ind[i])
            resh.append(row)
            row = []
    if len(row)!=0:
        resh.append(row)
    return np.array(resh)


def roi_cut(volume, seed_ind, centers, h_pad=70, v_pad=10):
    '''Cuts seperate rois for vertebrae'''

    vert_list = reshape_seeds(seed_ind)
    lens = [len(l) for l in vert_list]
    avg_len = np.mean(lens)

    vert_rois = []
    vert_roi_seeds = []
    vert_roi_starts = []
    vert_roi_ends = []
    for i in xrange(len(vert_list)):
        vert = vert_list[i]
        vert_len = lens[i]
        avg_pad = int(avg_len-vert_len)
        if avg_pad < 0:
            avg_pad = 0
        temp = vert_roi_cut(volume, vert, centers[vert], h_pad, v_pad+avg_pad/2)
        vert_rois.append(temp[0])
        vert_roi_seeds.append(temp[1])
        vert_roi_starts.append(temp[2])
        vert_roi_ends.append(temp[3])
    return vert_rois, vert_roi_seeds, vert_roi_starts, vert_roi_ends


def vert_roi_cut(volume, seed_ind, seed_p, h_pad, v_pad):
    '''Cuts individual vertebra'''

    max_X = volume.shape[0]-1
    max_Y = volume.shape[1]-1
    max_Z = volume.shape[2]-1

    x = [p[0] for p in seed_p]
    y = [p[1] for p in seed_p]
    centroid = (sum(x) / len(seed_p), sum(y) / len(seed_p))

    x_low_lim = centroid[0]-h_pad
    x_high_lim = centroid[0]+h_pad
    if x_low_lim < 0:
        x_low_lim = 0
    if x_high_lim > max_X:
        x_high_lim = max_X

    y_low_lim = centroid[1]-h_pad
    y_high_lim = centroid[1]+h_pad
    if y_low_lim < 0:
        y_low_lim = 0
    if y_high_lim > max_Y:
        y_high_lim = max_Y

    z_low_lim = np.min(seed_ind)-v_pad
    z_high_lim = np.max(seed_ind)+v_pad
    if z_low_lim < 0:
        z_low_lim = 0
    if z_high_lim > max_Z:
        z_high_lim = max_Z

    roi = volume[y_low_lim:y_high_lim, x_low_lim:x_high_lim, z_low_lim:z_high_lim]
    new_x = [p[0]-x_low_lim for p in seed_p]
    new_y = [p[1]-y_low_lim for p in seed_p]
    roi_seeds = np.stack((new_x, new_y), axis=1)

    roi_index = np.array([np.array(seed_ind)-z_low_lim])
    roi_seeds = np.concatenate((roi_seeds, roi_index.T), axis=1)
    starter_point = (y_low_lim, x_low_lim, z_low_lim)
    ending_point = (y_high_lim, x_high_lim, z_high_lim)

    return roi, roi_seeds, starter_point, ending_point
